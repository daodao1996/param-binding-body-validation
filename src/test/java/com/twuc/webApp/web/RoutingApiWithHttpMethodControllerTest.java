package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
class RoutingApiWithHttpMethodControllerTest {

	@Autowired
	private MockMvc mockMvc;
	private Student student;
	private List<Student> studentsList;
	@Autowired
	private RoutingApiWithHttpMethodController routingApiWithHttpMethodController;

	@BeforeEach
	void setUp(){
		student = new Student("xiaowang",18,"female","123455@21.dw");
		studentsList = new ArrayList<>();
		studentsList.add(new Student("xiaozheng",18,"female","3122@42342.d"));
		studentsList.add(new Student("xiaohua",20,"male","e3243243@dfa.w"));
		studentsList.add(new Student("xiaosong",55,"female","6869@fd.fd"));
	}

	@Test
	void add_name() throws Exception {
		mockMvc.perform(post("/api/add/name").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content("dingyunpeng"))
				.andExpect(content().string("dingyunpeng"));
	}

	@Test
	void add_age() throws Exception {
		mockMvc.perform(post("/api/add/age").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(String.valueOf(18)))
				.andExpect(content().string("18"));
	}

	@Test
	void add_student() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		String studentString = objectMapper.writeValueAsString(student);
		mockMvc.perform(post("/api/add/student").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(studentString))
				.andExpect(jsonPath("$.name").value("xiaowang"));
	}

	@Test
	void add_students_list() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		String studentsListString = objectMapper.writeValueAsString(studentsList);

		mockMvc.perform(post("/api/add/students").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(studentsListString))
				.andExpect(jsonPath("$[0].name").value("xiaozheng"))
				.andExpect(jsonPath("$[2].age").value("55"));
	}

	@Test
	void get_all_students() throws Exception {
		routingApiWithHttpMethodController.setStudentList(studentsList);
		mockMvc.perform(get("/api/get/students"))
				.andExpect(jsonPath("$[0].name").value("xiaozheng"));
	}

	@Test
	void delete_all_students() throws Exception {
		routingApiWithHttpMethodController.setStudentList(studentsList);
		mockMvc.perform(delete(
				"/api/delete/students"))
				.andExpect(content().string("3"));
	}

	@Test
	void should_return_error_when_student_name_is_null() throws Exception {
		Student studentNameIsNull = new Student(null,17,"male","fdfd@3.d");
		ObjectMapper objectMapper = new ObjectMapper();
		String studentString = objectMapper.writeValueAsString(studentNameIsNull);

//		mockMvc.perform(post("/api/add/student").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(studentString))
//				.andExpect(status().is5xxServerError());


		assertThrows(ValidationException.class,() -> mockMvc.perform(post("/api/add/student")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(studentString)));
	}

}
