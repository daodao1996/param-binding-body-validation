package com.twuc.webApp.web;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Student {

    @NotNull
    private String name;
    @Min(18)
    @Max(100)
    @JsonIgnore
    private Integer age;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String gender;
    @Email
    @JsonProperty("user_email")
    private String email;

    public Student(){

    }

    public Student(String name,Integer age,String gender, String email){
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    @JsonProperty("user_email")
//    @JsonProperty("email")
    public String getEmail() {
        return email;
    }
    @JsonProperty("email")
//    @JsonProperty("user_email")
    public void setEmail(String email) {
        this.email = email;
    }
}
