package com.twuc.webApp.web;

// TODO:
//
// 请参考 RoutingApiWithHttpMethodControllerTest 中的测试，创建四个 API。这 4 个 API 均有相似的形式。只是
// 支持的 HTTP METHOD 各自不同。
//
// 它们的 URI 形式均为 /api/routing/method
// 它们支持的 HTTP Method 为：GET/POST/DELETE/PUT，除此之外的 HTTP 方法均不支持。对于不支持的 HTTP Method 其
//   Status Code 为 404 或者 405 均可。
// 响应的 Content-Type：application/json;charset=UTF-8
// 响应的 Content 依据 HTTP Method 的不同分别为：
//
// | HTTP Method    | Status Code    | Content                                   |
// |----------------|----------------|-------------------------------------------|
// | GET            | 200            | { "value": "You are calling GET" }      |
// | PUT            | 200            | { "value": "You are calling PUT" }      |
// | POST           | 201            | { "value": "You are calling POST" }     |
// | DELETE         | 200            | { "value": "You are calling DELETE" }   |
//

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;

// <--start-
@RestController
@RequestMapping("/api")
public class RoutingApiWithHttpMethodController {

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    private List<Student> studentList = new ArrayList<>();

    @PostMapping("/add/name")
    public String addName(@RequestBody String name){
        return name;
    }

    @PostMapping("/add/age")
    public Integer addAge(@RequestBody Integer age){
        return age;
    }

    @PostMapping(value = "/add/student",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Student> addStudent(@RequestBody @Valid Student student, Errors errors) throws ValidationException {
        if(errors.hasErrors()){
            throw new ValidationException("valid failed");
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).header("request-id","123456").body(student);
    }

    @PostMapping(value = "/add/students",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Student> addStudents(@RequestBody List<Student> students){
        studentList.addAll(students);
        return studentList;
    }

    @GetMapping("/get/students")
    public List<Student> getAllStudents(){
        return studentList;
    }

    @DeleteMapping("/delete/students")
    public Integer deleteStudents(){
        int size = studentList.size();
        studentList.clear();
        return size;
    }

}
// --end->

